import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.bson.Document;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import com.mongodb.client.model.Filters;
import org.bson.types.Binary;

public class FileDeduplicator {
    private final DataProcessor dataProcessor = new DataProcessor();
    private final HashMap<ByteArrayWrapper, Integer> countOfSameHash = new HashMap<>();
    private final DatabaseConnector connector = new DatabaseConnector();
    private final LinkedList<ArrayList<byte[]>> linkedFilesHashes = new LinkedList<>();
    private static final int BLOCK_SIZE = 10;

    private ArrayList<byte[]> splitByteArray(byte[] data, int blockSize) {
        ArrayList<byte[]> result = new ArrayList<>();
        int start = 0;

        while (start < data.length) {
            int end = Math.min(data.length, start + blockSize);
            result.add(Arrays.copyOfRange(data, start, end));
            start += blockSize;
        }
        return result;
    }

    public void startDeduplication(ArrayList<File> files) throws NoSuchAlgorithmException {
        try {
            for (int i = 0; i < files.size(); i++) {

                byte[] fileBytes = fileToByteArrayOutStream(files.get(i)).toByteArray();
                ArrayList<byte[]> splittedFile = splitByteArray(fileBytes, BLOCK_SIZE);

                linkedFilesHashes.add(new ArrayList<>());
                ArrayList<byte[]> currentFileHashes = linkedFilesHashes.get(i);

                deduplicateFile(splittedFile, files.get(i), currentFileHashes);
                System.out.println();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ByteArrayOutputStream fileToByteArrayOutStream(File file) throws IOException {
        BufferedImage image = ImageIO.read(file);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(image, FilenameUtils.getExtension(file.getName()), bos);
        return bos;
    }

    private void deduplicateFile(ArrayList<byte[]> splittedFile, File file, ArrayList<byte[]> currentFileHashes)
            throws NoSuchAlgorithmException, IOException {
        MessageDigest shaDigest = MessageDigest.getInstance("SHA-256");
        String shaChecksum = DataProcessor.getFileChecksum(shaDigest, file);

        for (int i = 0; i < splittedFile.size(); ++i) {
            byte[] curHashCode = MessageDigest.getInstance("SHA-1").digest(splittedFile.get(i));

            currentFileHashes.add(curHashCode);
            if (connector.getCollection().find(Filters.eq("HashCode", curHashCode)).first() != null) {
                countOfSameHash.replace(wrap(curHashCode), countOfSameHash.get(wrap(curHashCode)) + 1);
            } else {
                Document doc = new Document()
                        .append("FileChecksum", shaChecksum)
                        .append("HashCode", new Binary(curHashCode))
                        .append("Block", splittedFile.get(i))
                        .append("Num", i);

                connector.getCollection().insertOne(doc);
                countOfSameHash.put(wrap(curHashCode), 1);
            }
        }
    }

    public void startRecovery() throws IOException {
        for (int i = 0; i < linkedFilesHashes.size(); i++) {
            recoveryFile(linkedFilesHashes.get(i));
        }
    }

    private void recoveryFile(ArrayList<byte[]> fileHashes) throws IOException {
        String fileName = String.format("%s.%s", RandomStringUtils.randomAlphanumeric(8), ".jpg");
        Path outputFilePath = Files.createFile(Path.of("./src/main/resources/output_data/" + fileName));

        for (byte[] hash : fileHashes) {
            Document document = connector.getCollection().find(Filters.eq("HashCode", hash)).first();

            Binary block = (Binary) document.get("Block");
            dataProcessor.saveFile(outputFilePath, block.getData());
        }
    }

    private static ByteArrayWrapper wrap(byte[] inputArray) {
        return new ByteArrayWrapper(inputArray);
    }
}
