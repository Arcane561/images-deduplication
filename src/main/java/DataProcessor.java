import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class DataProcessor {
    private static String DATA_PATH = "./src/main/resources/input_data";
    private static final String[] IMAGES_EXTENSION = {"png", "jpg"};
    private static final File directory = new File(DATA_PATH);


    static final FilenameFilter imagesFilter = new FilenameFilter() {

        @Override
        public boolean accept(File file, String s) {
            for (final String extension : IMAGES_EXTENSION) {
                if (s.endsWith("." + extension)) {
                    return true;
                }
            }
            return false;
        }
    };

    public ArrayList<File> loadFiles() {
        ArrayList<File> files = new ArrayList<>();
        if (directory.isDirectory()) {
            Collections.addAll(files, Objects.requireNonNull(directory.listFiles(imagesFilter)));
            return files;
        }
        return null;
    }

    public void saveFile(Path path, byte[] data) throws IOException {
        Files.write(path, data, StandardOpenOption.APPEND);
    }

    public static String getFileChecksum(MessageDigest digest, File file) throws IOException
    {
        FileInputStream fis = new FileInputStream(file);

        byte[] byteArray = new byte[1024];
        int bytesCount = 0;

        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        };

        fis.close();

        byte[] bytes = digest.digest();

        StringBuilder sb = new StringBuilder();

        for (byte aByte : bytes) {
            sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }
}