import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class DatabaseConnector {
    private MongoDatabase database = null;
    private MongoCollection<Document> collection = null;

    DatabaseConnector() {
        MongoClient mongoClient = new MongoClient("192.168.1.70", 27017);
        database = mongoClient.getDatabase("image_deduplication");
        collection = database.getCollection("Data");
    }

    public MongoDatabase getDatabase() {
        return database;
    }

    public MongoCollection<Document> getCollection() {
        return collection;
    }
}
