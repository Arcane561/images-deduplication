import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class Main {

    public static void main(String[] args) {
//        DatabaseConnector databaseConnector = new DatabaseConnector();
//        databaseConnector.createConnection();
        try {
            FileDeduplicator fileDeduplicator = new FileDeduplicator();
            DataProcessor dataProcessor = new DataProcessor();
            ArrayList<File> files = dataProcessor.loadFiles();

            fileDeduplicator.startDeduplication(files);

            fileDeduplicator.startRecovery();

        } catch (IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
